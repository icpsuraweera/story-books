import './App.css';
import { Button } from "@blueprintjs/core";
import React, { useState } from 'react';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom'
import About from './About'


const App =() => {

  const [count, setCount] = useState(0);
  const [display, setdisplay] = useState(0);
const submitHandle=(e)=>{
  e.preventDefault();
}
  const inputHandle1=(e)=>{
     //setCount(count+1)
     setCount(e.target.value)
     setdisplay(e.target.value)
  }
  const buttonclick=()=>{
    setdisplay(count)
    alert("your typed : "+ display)
  }
  const buttonClear=()=>{
    setdisplay('')
  }
  return (
    <Router>
    <div className="App">
      <header className="App-header">
        <form  onSubmit={submitHandle}>
          <h1>{display}</h1>
         <input type="text" onChange={inputHandle1} />
        <Button onClick={buttonclick} type="submit" intent="success" text="button content"  />
        <Button onClick={buttonClear} type="reset" intent="success" text="button content"  />
        
        <Link exact  to="/about">About</Link>
        </form>
      </header>
    </div>
    <Route path="/about" exact component={About}></Route>
   </Router>
  );
}

export default App;
